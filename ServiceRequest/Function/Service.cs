using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceRequest.DTO;
using ServiceRequest.Helper;

namespace ServiceRequest.Function
{
    public class Service
    {
        [FunctionName("GetAllRequest")]
        public IActionResult GetAllRequest(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "servicerequest")] 
            HttpRequest req,
            ILogger log)
        {
            try
            {
                var response = ExampleList.service;
                if (response.Count > 0)
                {
                    return new OkObjectResult(response);
                }
                else
                {
                    return new ContentResult
                    {
                        StatusCode = (int)HttpStatusCode.NonAuthoritativeInformation,
                    };
                }
            }
            catch (Exception e)
            {
                return ExceptionHandler.Handle(e);
               
            }
        }

        [FunctionName("GetRequestById")]
        public IActionResult GetRequestById(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "servicerequest/{id}")]
            HttpRequest req,
           string requestId,
           ILogger log)
        {
            try
            {
                var response = CheckRequestIdandReturn(requestId);
                

                if (response == null)
                    throw new NotFoundHttpException($"Request with id {requestId} not found");
                return new JsonResult(response);
            }
            catch (Exception e)
            {
                return ExceptionHandler.Handle(e);
            }
        }

        private ServiceObject CheckRequestIdandReturn(string requestId)
        {
            if (!Guid.TryParse(requestId, out var requestGuid))
                throw new Exception($"Unable to parse guid \"{requestGuid}\"");
            return ExampleList.service.FirstOrDefault(x => x.Id == requestGuid);
        }

        [FunctionName("CreateRequest")]
        public async Task<IActionResult> CreatRequest(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "servicerequest")]
            HttpRequest req,
           ILogger log)
        {
            try
            {
                var result = await req.ReadAsStringAsync();
                var request = JsonConvert.DeserializeObject<ServiceObject>(result);

                ExampleList.Record(request); ;

                return new ContentResult
                {
                    StatusCode = (int)HttpStatusCode.Created
                };
            }
            catch (Exception e)
            {
                return new ContentResult
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Content = e.Message
                };
            }
        }

        [FunctionName("UpdateRequest")]
        public async Task<IActionResult> UpdateRequest(
          [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "servicerequest/{requestId}")]
            HttpRequest req,
            string requestId,
          ILogger log)
        {
            try
            {
                var result = await req.ReadAsStringAsync();
                var request = JsonConvert.DeserializeObject<ServiceObject>(result);

                var response = CheckRequestIdandReturn(requestId);
                response.BuildingCode = request.BuildingCode;
                response.CurrentStatus = response.CurrentStatus;
                response.Description = request.Description;
                response.LastModifiedBy = request.LastModifiedBy;
                response.LastModifiedDate = request.LastModifiedDate;
                return new OkResult();
            }
            catch (Exception e)
            {
                return new ContentResult
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Content = e.Message
                };
            }
        }

        [FunctionName("DeleteRequest")]
        public IActionResult DeleteRequest(
          [HttpTrigger(AuthorizationLevel.Anonymous, "Delete", Route = "servicerequest/{requestId}")]
            HttpRequest req,
            string requestId,
          ILogger log)
        {
            try
            {

                var response = CheckRequestIdandReturn(requestId);
                ExampleList.service.Remove(response);
                return new OkResult();
            }
            catch (Exception e)
            {
                return new ContentResult
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Content = e.Message
                };
            }
        }
    }
    public static class ExampleList
    {
        public static List<ServiceObject> service = new List<ServiceObject>
        {   new ServiceObject(){
                    Id = new Guid("727b376b-79ae-498e-9cff-a9f51b848ea4"),
                    BuildingCode = "COH",
                    Description = "Please turn up the AC in suite 1200D. It is too hot here.",
                    CurrentStatus = CurrentStatus.Created,
                    CreatedBy = "Nik Patel",
                    CreatedDate = Convert.ToDateTime("2019-08-01T14:25:43.511Z"),
                    LastModifiedBy = "Jane Doe",
                    LastModifiedDate = Convert.ToDateTime("2019-08-01T15:01:23.511Z")
            },

        };

        public static void Record(ServiceObject value)
        {
            service.Add(value);
        }
    }
}

