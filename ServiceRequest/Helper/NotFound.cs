﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ServiceRequest.Helper
{
    public class NotFoundHttpException : HttpException
    {
        public NotFoundHttpException(string errorMessage) : base(HttpStatusCode.NotFound, errorMessage)
        {
        }
    }
}
