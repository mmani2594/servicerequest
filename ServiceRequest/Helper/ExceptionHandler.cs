﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ServiceRequest.Helper
{
    public class ExceptionHandler
    {
        public static IActionResult Handle(Exception e)
        {
            if (e is HttpException hex)
                return hex.ToActionResult();

            var obj = new JObject
            {
                ["status"] = "error",
                ["message"] = e.Message,
            };

            return new JsonResult(obj)
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
        }

    }
}