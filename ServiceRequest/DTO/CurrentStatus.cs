﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceRequest.DTO
{
    public enum CurrentStatus
    {
        NotApplicable,
        Created,
        InProgress,
        Completed,
        Cancelled
    }
}
