﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace ServiceRequest.Helper
{
    public abstract class HttpException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public string ErrorMessage { get; }

        public HttpException(HttpStatusCode statusCode, string errorMessage)
        {
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
        }

        internal IActionResult ToActionResult()
        {
            throw new NotImplementedException();
        }
    }
}