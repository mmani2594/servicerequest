﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceRequest.DTO
{
    public class ServiceObject
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("buildingCode")]
        public string BuildingCode { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }


        [JsonProperty("currentStatus")]
        public CurrentStatus CurrentStatus { get; set; }

        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("lastModifiedBy")]
        public string LastModifiedBy { get; set; }

        [JsonProperty("lastModifiedDate")]
        public DateTime LastModifiedDate { get; set; }
    }
}
